﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;

namespace LibProject.WowProcess
{
    public static class ViewModeProcess
    {
        public class ViewModelProc
        {
            public string Name { get; set; }
            public int Id { get; set; }
            public Process source { get; set; }

        }

        public static ViewModelProc ConvertProcInViewModel(Process proc)
        {
            return new ViewModelProc() { Id = proc.Id, Name = proc.ProcessName, source = proc };
        }

        public static List<ViewModelProc> ConvertProcesInViewModel(Process[] procs)
        {
            List<ViewModelProc> result = new List<ViewModelProc>();
            foreach (var proc in procs)
            {
                result.Add(ConvertProcInViewModel(proc));
            }
            return result;
        }
    }
}
