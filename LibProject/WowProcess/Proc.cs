﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Magic;

namespace LibProject.WowProcess
{
    public static class Proc
    {

        [DllImport("user32.dll")]
        static public extern bool SetForegroundWindow(IntPtr hWnd);

        private static Process _activeProcess;

        public static Process GetActiveProcess()
        {
            return _activeProcess;
        }

        public static void SetActiveProcess(Process proc)
        {
            _activeProcess = proc;
        }
        public static List<int> GetProcessesId(string nameProc)
        {
            List<int> IdProc=new List<int>();
            Process[] procList = Process.GetProcessesByName("WoW");
            foreach (var process in procList)
            IdProc.Add(process.Id);
            return IdProc;
        }

        public static Process[] GetProcessesByName(string nameProc)
        {
            return Process.GetProcessesByName(nameProc);
        }
    }
}
