﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using LibProject;
namespace LibProject.LuaScripts
{
    public struct countemblems
    {
        public int triumf;
        public int zavoev;
        public int dobl;
        public int heroism;
    }
    public struct Vendor
    {
        public string Name;
        public float x, y, z;
        public UInt64 guid;
        public Emblem emblem;
    }
    public class Vendors : Dictionary<int,Vendor>
    {
        //х = 5941.325 y = 507.647156 z = 650.1792 Магистр Ламбрисса guid=17379391491880888708 4
        //х = 5937.31641 y = 504.816345 z = 650.178833 Магистр Бразайл guid=17379391491864111493 3
        //х = 5932.229 y = 507.560516 z = 650.178833 Магистр Сириен guid=17379391531827425731 2
        //х = 5934.73828 y = 510.532349 z = 650.178833 Магистр Весара guid=17379391557530157360 1
        //х = 5937.06445 y = 509.3245 z = 650.178833 Магистр Арлан guid=17379391598567174064 0
        public Vendors()
        {
            this.Add(4,new Vendor()
            {
                guid = 17379391491880888708,
                Name = "Магистр Ламбрисса",
                x = (float)5941.325,
                y = (float)507.647156,
                z = (float)650.1792,
                emblem = new Emblem("-",0)
            });

            this.Add(3,new Vendor()
            {
                guid = 17379391491864111493,
                Name = "Магистр Бразайл",
                x = (float)5937.31641,
                y = (float)504.816345,
                z = (float)650.178833,
                emblem = new Emblem("Эмблема героизма",0)
            });

            this.Add(2,new Vendor()
            {
                guid = 17379391531827425731,
                Name = "Магистр Сириен",
                x = (float)5932.229,
                y = (float)507.560516,
                z = (float)650.178833,
                emblem = new Emblem("Эмблема доблести",0)
            });

            this.Add(1,new Vendor()
            {
                guid = 17379391557530157360,
                Name = "Магистр Весара",
                x = (float)5934.73828,
                y = (float)510.532349,
                z = (float)650.178833,
                emblem = new Emblem("Эмблема завоевания",0)
            });

            this.Add(0,new Vendor()
            {
                guid = 17379391598567174064,
                Name = "Магистр Арлан",
                x = (float)5937.06445,
                y = (float)509.3245,
                z = (float)650.178833,
                emblem = new Emblem("Эмблема триумфа",0)
            });

         
        }

        public void SetEmblem(countemblems emblems)
        {
            this[0].emblem.count = emblems.triumf;
            this[1].emblem.count = emblems.zavoev;
            this[2].emblem.count = emblems.dobl;
            this[3].emblem.count = emblems.heroism;
        }

    }
}
