﻿using System;
using System.Text;
using System.Threading;
using LibProject.WowProcess;

namespace LibProject.LuaScripts
{
    public class Lua
    {

        private Hook MyHook;
        public Lua(int ProcessId)
        {
            MyHook=new Hook((uint)ProcessId);
        }
          public void LuaDoString(string command)
        {
            // Allocate memory
            uint DoStringArg_Codecave = MyHook.Memory.AllocateMemory(Encoding.UTF8.GetBytes(command).Length + 1);
            // offset:
            uint FrameScript__Execute = 0x00819210;


            // Write value:
            MyHook.Memory.WriteBytes(DoStringArg_Codecave, Encoding.UTF8.GetBytes(command));

            // Write the asm stuff for Lua_DoString
            String[] asm = new String[] 
            {
                "mov eax, " + DoStringArg_Codecave,
                "push 0",
                "push eax",
                "push eax",
                "mov eax, " + (uint)FrameScript__Execute, // Lua_DoString
                "call eax",
                "add esp, 0xC",
                "retn",    
            };

            // Inject
            MyHook.InjectAndExecute(asm);
            // Free memory allocated 
            MyHook.Memory.FreeMemory(DoStringArg_Codecave);
        }

        public string GetLocalizedText(string Commandline)
        {
            // Command to send using LUA
            String Command = Commandline;

            // Allocate memory for command
            uint Lua_GetLocalizedText_Space = MyHook.Memory.AllocateMemory(Encoding.UTF8.GetBytes(Command).Length + 1);

            // offset:
            uint ClntObjMgrGetActivePlayerObj = 0x004038F0;
            uint FrameScript__GetLocalizedText = 0x007225E0;

            // Write command in the allocated memory
            MyHook.Memory.WriteBytes(Lua_GetLocalizedText_Space, Encoding.UTF8.GetBytes(Command));

            String[] asm = new String[] 
            {
            "call " + (uint)ClntObjMgrGetActivePlayerObj,
            "mov ecx, eax",
            "push -1",
            "mov edx, " + Lua_GetLocalizedText_Space + "",
            "push edx",
            "call " + (uint)FrameScript__GetLocalizedText,
            "retn",
            };
            // Inject the shit
            string sResult = Encoding.ASCII.GetString(MyHook.InjectAndExecute(asm));

            // Free memory allocated for command
            MyHook.Memory.FreeMemory(Lua_GetLocalizedText_Space);

            // Uninstall the hook
            return sResult;
        }
        //х = 5941.325 y = 507.647156 z = 650.1792 Магистр Ламбрисса guid=17379391491880888708 4
        //х = 5937.31641 y = 504.816345 z = 650.178833 Магистр Бразайл guid=17379391491864111493 3
        //х = 5932.229 y = 507.560516 z = 650.178833 Магистр Сириен guid=17379391531827425731 2
        //х = 5934.73828 y = 510.532349 z = 650.178833 Магистр Весара guid=17379391557530157360 1
        //х = 5937.06445 y = 509.3245 z = 650.178833 Магистр Арлан guid=17379391598567174064 0

        public void ClickAndMove(float x,float y,float z)
        {

            MyHook.Memory.WriteFloat(((uint)0x8C + (uint)0x00CA11D8), x);
            MyHook.Memory.WriteFloat(((uint)0x90 + (uint)0x00CA11D8), y);
            MyHook.Memory.WriteFloat(((uint)0x94 + (uint)0x00CA11D8), z);
            MyHook.Memory.WriteInt(((uint)0x1C + (uint)0x00CA11D8), 4);
        }

        public void RunCTM()
        {
            String[] ASM = new String[]
            {
            "mov edx, [" + (uint)0xBD08F4 + "]",
            "add edx, " + (uint)0x30,
            "mov ecx, 1",
            "mov [edx], ecx",
            "@out:",
            "retn",
            };
            MyHook.InjectAndExecute(ASM);
        }
        public void InteractVenderandPayEmblem(UInt64 guidvendor, string NameEmblem, int count,int Time)
        {
            string LuaCommand =
                "local function buy (n,q) for i=1,100 do if n==GetMerchantItemInfo(i) then BuyMerchantItem(i,q) end end end buy (\"" +
                NameEmblem + "\"," + count.ToString() + ")";
            MyHook.Memory.WriteUInt64(0x00BD07B0, guidvendor);
            LuaDoString("InteractUnit(\"target\");");
            Thread.Sleep(Time);
            LuaDoString(LuaCommand);
        }


        public void Obmen(countemblems emblem,int Time)
        {
            Vendors V=new Vendors();
            V.SetEmblem(emblem);
            RunCTM();
            for (int i = 0; i < V.Count-1; i++)
            {
                ClickAndMove(V[i].x, V[i].y, V[i].z);
                Thread.Sleep(Time);
                InteractVenderandPayEmblem(V[i].guid, V[i].emblem.Name, V[i].emblem.count,Time);
            }

        }

        #region Мусор
        private void Trash()
        {
            var playerbase = MyHook.Memory.ReadUInt(MyHook.Memory.ReadUInt(MyHook.Memory.ReadUInt((uint)0x00CD87A8) + 0x34) + 0x24);
            var TargetGUID = MyHook.Memory.ReadUInt64(0x00BD07B0);
            UInt64 t = 0xF130009435012FB0; 
            //MyHook.Memory.WriteUInt64(0x00BD07B0, t);
            //LuaDoString("InteractUnit(\"target\");");
            var firstobj = MyHook.Memory.ReadUInt(0xC79CE0 + 0xAC);
            float x1 = MyHook.Memory.ReadFloat(playerbase + 0x798); //5837
            float y1 = MyHook.Memory.ReadFloat(playerbase + 0x79C); //729
            float z1 = MyHook.Memory.ReadFloat(playerbase + 0x7A0); //643
        }
        
        public void GoTo(float X, float Y, float Z, UInt64 GUID, int ActionType)
        {
            var wow = MyHook.Memory;
            uint ctmBase = (uint)0x007278B8;
            uint ctmTurnScale = ctmBase + 0x04;
            uint ctmDistance = ctmBase + 0x0c;
            uint ctmAction = ctmBase + 0x1c;
            uint ctmTarget = ctmBase + 0x20;
            uint ctmDestinationX = ctmBase + 0x8C;
            uint ctmDestinationY = ctmDestinationX + 0x4;
            uint ctmDestinationZ = ctmDestinationY + 0x4;
            switch (ActionType)
            {
                case 0:
                    wow.WriteUInt64(ctmTarget, 0);
                    wow.WriteFloat(ctmDistance, 0f); // ctm distance
                    wow.WriteFloat(ctmDestinationX, 0);//ctm x
                    wow.WriteFloat(ctmDestinationY, 0);//ctm y
                    wow.WriteFloat(ctmDestinationZ, 0);//ctm z
                    break;
                case 1:
                    wow.WriteUInt64(ctmTarget, GUID);
                    wow.WriteFloat(ctmDistance, 30.5f); // ctm distance
                    break;
                case 3:
                    wow.WriteUInt64(ctmTarget, 0);
                    wow.WriteFloat(ctmDistance, 30.5f); // ctm distance
                    break;
                case 4:
                   
                    wow.WriteFloat(ctmDestinationX, X);//ctm x
                    wow.WriteFloat(ctmDestinationY, Y);//ctm y
                    wow.WriteFloat(ctmDestinationZ, Z);//ctm z
                    wow.WriteFloat(ctmDistance, 0.5f); // ctm distance
                    break;
                case 6:
                    wow.WriteUInt64(ctmTarget, GUID);
                    wow.WriteFloat(ctmDistance, 3.66666650772095f); // ctm distance
                    break;
                case 7:
                    wow.WriteUInt64(ctmTarget, GUID);
                    wow.WriteFloat(ctmDistance, 3.66666650772095f); // ctm distance
                    break;
                case 9:
                    wow.WriteUInt64(ctmTarget, GUID);
                    wow.WriteFloat(ctmDistance, 3.66666650772095f); // ctm distance
                    break;
                case 11:
                    wow.WriteUInt64(ctmTarget, GUID);
                    wow.WriteFloat(ctmDistance, 3.66666650772095f); // ctm distance
                    break;
            }
            wow.WriteInt(ctmAction, ActionType); // ctm action
        }
        #endregion

    }
}
