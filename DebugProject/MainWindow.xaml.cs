﻿using System.Windows;


namespace WowServerList
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            SetProcessWoW window=new SetProcessWoW();
            window.ShowDialog();
        }
    }
}
