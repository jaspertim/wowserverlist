﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LibProject;
using LibProject.LuaScripts;
using LibProject.WowProcess;

namespace TradeEmblem
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

        }



        private void Button_Click(object sender, RoutedEventArgs e)
        {
            int led = Convert.ToInt32(TextBoxCountLed.Text);
            int triumf = Convert.ToInt32(TextBoxCountTriumf.Text);
            int zavoev = Convert.ToInt32(TextBoxCountZavoev.Text);
            int doblesti = Convert.ToInt32(TextBoxCountDoblesti.Text);
            int time = Convert.ToInt32(TextBoxTime.Text);
            EnterEmblem(led,triumf,zavoev,doblesti,time);
            Close();
        }
        
        void EnterEmblem(int led, int triumf, int zavoev, int dobl, int Time)
        {
            Lua L = new Lua(Proc.GetActiveProcess().Id);
            L.Obmen(new countemblems()
            {
                triumf = led,
                zavoev = led + triumf,
                dobl = led + triumf + zavoev,
                heroism = led + triumf + zavoev + dobl
            }, Time);
        }
    }

           
}
