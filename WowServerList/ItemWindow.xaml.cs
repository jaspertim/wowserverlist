﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WowServerList
{
    /// <summary>
    /// Логика взаимодействия для ItemWindow.xaml
    /// </summary>
    public partial class ItemWindow : Window
    {
        Base B=new Base();
        private int _id;
        private ViewElement _viewElement;
        private ItemChoice _choice;
        public ItemWindow(ItemChoice choice,int idelement,ViewElement view)
        {
            InitializeComponent();
            this.B = Base.InsBase;
            this._id = idelement;
            this._viewElement = view;
            this._choice = choice;
            //Забиндованная скрытая команда
            //=============================================================
            CommandBinding bind =new CommandBinding(ApplicationCommands.Save);
            bind.Executed += CommandBinding_OnExecuted;
            this.CommandBindings.Add(bind);

            KeyBinding key = new KeyBinding()
            {
                Command = ApplicationCommands.Save,
                Gesture = new KeyGesture(Key.Z,ModifierKeys.Control)
            };
            this.InputBindings.Add(key);
            //=============================================================
            switch (_choice)
            {
                    case ItemChoice.Add:
                    Enter.Content = "Добавить";
                    this.Title = "Добавить элемент";
                    Uri iconUriAdd = new Uri("pack://application:,,,/Icons/gtk-add_5273.ico", UriKind.RelativeOrAbsolute);
                    this.Icon = BitmapFrame.Create(iconUriAdd);
                    break;
                    
                    case ItemChoice.Edit:
                    Enter.Content = "Редактировать";
                    this.Title = "Редактировать элемент";
                    TextBoxLogin.Text = B.list[_id].login;
                    TextBoxPassword.Password = B.list[_id].pass;
                    Uri iconUriChange = new Uri("pack://application:,,,/Icons/edit_2944.ico", UriKind.RelativeOrAbsolute);
                    this.Icon = BitmapFrame.Create(iconUriChange);
                    break;
            }           
        }
        /// <summary>
        /// Функция копирования поля passwordbox в textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PasswordBox_OnPasswordChanged(object sender, RoutedEventArgs e)
        {
            TextBoxViewPassword.Text = TextBoxPassword.Password;
        }
        /// <summary>
        /// Бинд команды ctrl+z секретный бинд :D
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CommandBinding_OnExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            if (TextBoxViewPassword.Visibility == Visibility.Hidden)
            {
                TextBoxViewPassword.Visibility = Visibility.Visible;
            }
            else TextBoxViewPassword.Visibility = Visibility.Hidden;
        }
        /// <summary>
        /// Кнопка принять изменения, добавления
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Enter_Click(object sender, RoutedEventArgs e)
        {
            switch (_choice)
            {
                    case ItemChoice.Add:
                        if(_viewElement==ViewElement.Users)
                        {
                            loginPass lp=new loginPass()
                            {
                                login = TextBoxLogin.Text,
                                pass = TextBoxPassword.Password
                            };
                            B.AddloginPass(lp);
                        }
                    break;
                    case ItemChoice.Edit:
                        if(_viewElement==ViewElement.Users)
                        {
                            loginPass lp=new loginPass()
                            {
                                login = TextBoxLogin.Text,
                                pass = TextBoxPassword.Password
                            };
                            B.Changelist(_id,lp);
                        }
                    break;
            }
            Close();
        }
    }
}
