﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using OpenFileDialog = Microsoft.Win32.OpenFileDialog;

namespace WowServerList
{
    /// <summary>
    /// Логика взаимодействия для Options.xaml
    /// </summary>
    public partial class OptionsWindow : Window
    {
        public OptionsWindow()
        {
            InitializeComponent();
            TextBoxPathWoW.Text = Option.InsOption.FolderWoW;
            TextBoxTime.Text = Option.InsOption.StartTime.ToString();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Option.InsOption.FolderWoW = TextBoxPathWoW.Text;
            Option.InsOption.StartTime = Convert.ToInt64(TextBoxTime.Text);
            SerializeXml Sxml=new SerializeXml();
            Sxml.SerializeToXML(Option.InsOption,Sxml.FilenameOption);
            Close();
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog dlg=new FolderBrowserDialog();
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) TextBoxPathWoW.Text = dlg.SelectedPath;
        }
    }
}
