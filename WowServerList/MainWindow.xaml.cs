﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Xml.Serialization;
using LibProject.WowProcess;
using TradeEmblem;
using WowServerList.Properties;
using MessageBox = System.Windows.MessageBox;
using MouseEventArgs = System.Windows.Input.MouseEventArgs;
namespace WowServerList
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const uint WM_KEYDOWN = 0x0100;
        private const uint WM_KEYUP = 0x0101;
        private const uint WM_CHAR = 0x0102;
        private const int VK_RETURN = 0x0D;
        private const int VK_TAB = 0x09;
        private Process curProc; //Текущий процесс
        private SerializeXml Sxml;

        private Base B
        {
            get { return Base.InsBase; }
            set { Base.InsBase = value; }
        }

        private Option option
        {
            get { return Option.InsOption; }
            set { Option.InsOption = value; }
        }
        public MainWindow()
        {
            InitializeComponent();
            Sxml=new SerializeXml();
            B = (Base)Sxml.DeserializeFromXML(B, Sxml.FilenameBase);
            option = (Option)Sxml.DeserializeFromXML(option, Sxml.FilenameOption);
            UpdataComboBox();
        }
        /// <summary>
        /// Обновить список пользователей и серверов в ComboBox
        /// </summary>
        public void UpdataComboBox()
        {
            ComboBox1.Items.Clear();
            ComboBox2.Items.Clear();
            foreach (var server in B.ServerList)
            {
                ComboBox1.Items.Add(server);
            }
            ComboBox1.SelectedIndex = Option.InsOption.CurrientPositionServer;


            foreach (var loginpass in B.list)
            {
                ComboBox2.Items.Add(loginpass.login);
            }
            ComboBox2.SelectedIndex = Option.InsOption.CurrientPositionLoginPass;
        }

        /// <summary>
        /// Смена сервера в игре WoW
        /// </summary>
        /// <param name="server"> Текст сервера</param>
        public void ChangeServer(string server)
        {
            using (Stream s = File.OpenWrite(Option.InsOption.FolderWoW+@"\Data\ruRU\realmlist.wtf"))
            {
               StreamWriter SW=new StreamWriter(s);
               SW.WriteLine("set realmlist " + server); 
                SW.Close();
                s.Close();
            }
            
        }

        public void StartWow(string WoW)
        {
            if (System.IO.File.Exists(WoW))
                curProc = Process.Start(WoW);
            else MessageBox.Show("Файл не найден WoW");
        }
        /// <summary>
        /// Кнопка запуска WoW
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Option.InsOption.CurrientPositionServer = ComboBox1.SelectedIndex;
            Option.InsOption.CurrientPositionLoginPass = ComboBox2.SelectedIndex;
            Sxml = new SerializeXml();
            Sxml.SerializeToXML(Option.InsOption, Sxml.FilenameOption);
            ChangeServer(ComboBox1.Items[ComboBox1.SelectedIndex].ToString());
            switch (MessageBox.Show("Открыть новое окно Wow? \nВ противном случае, будет предложен выбор уже открытых окон Wow.","Открыть игру?",MessageBoxButton.YesNoCancel,MessageBoxImage.Question,MessageBoxResult.Cancel))
            {
                 case MessageBoxResult.Yes:
                    StartWow(Option.InsOption.FolderWoW + @"\wow.exe");
                    break;

                 case MessageBoxResult.No:
                        if (Proc.GetProcessesByName("Wow").Length <= 0)
                        {
                            MessageBox.Show("Открытых окон Wow не найдено. Откройте новое окно.", "Ошибка",
                            MessageBoxButton.OK, MessageBoxImage.Error);
                            return;
                        }                    
                    SetProcessWoW windowSetProcessWoW=new SetProcessWoW();
                        if (windowSetProcessWoW.ShowDialog() == true)
                        {
                            curProc = Proc.GetActiveProcess();
                        }
                        else
                        {
                            return;
                        }
                    break;
                default:
                    return;
                    break;
            }

            try
            {
                DirectoryInfo dirInfo = new DirectoryInfo(Option.InsOption.FolderWoW + @"\Cache");
                dirInfo.Delete(true);
            }
            catch
            {

            }
            EnterLoginPassword(curProc, B.list[ComboBox2.SelectedIndex].login, B.list[ComboBox2.SelectedIndex].pass);
        }
        /// <summary>
        /// Функция перехвата управления игры WoW для ввода login и password.
        /// </summary>
        /// <param name="process">Запущенный процесс игры WoW</param>
        /// <param name="login">Имя пользователя</param>
        /// <param name="password">Пароль пользователя</param>
        public void EnterLoginPassword(Process process,string login, string password)
        {
            //Thread.Sleep(600);         
            new Thread(() =>
                {
                    try
                    {
                        Thread.CurrentThread.IsBackground = true;

                        while (!process.WaitForInputIdle()) Thread.Sleep(1000);

                        Thread.Sleep(new TimeSpan(Option.InsOption.StartTime*10000));
                        foreach (char accNameLetter in login)
                        {
                            SendMessage(process.MainWindowHandle, WM_CHAR, new IntPtr(accNameLetter), IntPtr.Zero);
                            Thread.Sleep(30);
                        }

                        //! Switch to password field
                        if (!String.IsNullOrWhiteSpace(password))
                        {
                            SendMessage(process.MainWindowHandle, WM_KEYUP, new IntPtr(VK_TAB), IntPtr.Zero);
                            SendMessage(process.MainWindowHandle, WM_KEYDOWN, new IntPtr(VK_TAB), IntPtr.Zero);

                            foreach (char accPassLetter in password)
                            {
                                SendMessage(process.MainWindowHandle, WM_CHAR, new IntPtr(accPassLetter), IntPtr.Zero);
                                Thread.Sleep(30);
                            }

                            //! Login to account
                            SendMessage(process.MainWindowHandle, WM_KEYUP, new IntPtr(VK_RETURN), IntPtr.Zero);
                            SendMessage(process.MainWindowHandle, WM_KEYDOWN, new IntPtr(VK_RETURN), IntPtr.Zero);

                        }

                        Thread.CurrentThread.Abort();                   
                    }
                    catch
                    {
                        Thread.CurrentThread.Abort();
                    }
                }).Start();
        }
        
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern bool SetForegroundWindow(IntPtr hWnd);
        [DllImport("user32.dll", EntryPoint = "SendMessage", CharSet = CharSet.Unicode)]
        public static extern int SendMessage(IntPtr hwnd, uint msg, IntPtr wParam, IntPtr lParam);
        //Выход из программы
        private void ButtonExit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
        /// <summary>
        /// Список пользователей и серверов. Кнопка.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonListUsers_Click(object sender, RoutedEventArgs e)
        {
            ListItems LI=new ListItems();            
            LI.ShowDialog();
            this.B = LI.b;
            UpdataComboBox();
        }

        private void ButtonListServers_OnClick(object sender, RoutedEventArgs e)
        {
            OptionsWindow O=new OptionsWindow();
            O.ShowDialog();
            
        }

        private void ButtonTradeEmblem_Click(object sender, RoutedEventArgs e)
        {
            TradeEmblem.MainWindow trade=new TradeEmblem.MainWindow();
            trade.ShowDialog();
        }

        private void ButtonSetProcess_OnClick(object sender, RoutedEventArgs e)
        {
            if (Proc.GetProcessesByName("Wow").Length <= 0)
            {
                MessageBox.Show("Открытых окон Wow не найдено. Откройте новое окно.", "Ошибка",
                MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            SetProcessWoW windowSetProcessWoW = new SetProcessWoW();
            if (windowSetProcessWoW.ShowDialog() == true)
            {
                curProc = Proc.GetActiveProcess();
            }
            else
            {
                return;
            }
        }
    }
}
