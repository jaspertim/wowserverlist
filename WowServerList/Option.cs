﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
namespace WowServerList
{
    [Serializable]
    public class Option
    {
        private static Option O=new Option();
        [XmlElement("FolderWoW")]
        private string _folderwow = @"D:\Games\WoW_for_Aldoran_3.3.5\";
        [XmlElement("StartTime")]
        private Int64 _starttime=7000;
        [XmlElement("CurrientPositionServer")]
        private int _curientserver = -1;
        [XmlElement("CurrientPositionLoginPass")]
        private int _curientloginpass = -1;
      
        public string FolderWoW
        {
            get { return _folderwow;  } 
            set { _folderwow = value; }
        }

        public static Option InsOption
        {
            get { return O; }
            set { O = value; }
        }

        public int CurrientPositionServer
        {
            get
            {
                if (_curientserver >= 0 && _curientserver < Base.InsBase.ServerList.Count)
                    return _curientserver;
                else
                {
                    _curientserver = -1;
                    return _curientserver;
                }
                    
            }
            set
            {
                if (value >= 0 && value < Base.InsBase.ServerList.Count)
                    _curientserver = value;
                else _curientserver = -1;
            }
        }

        public int CurrientPositionLoginPass
        {
            get
            {
                if (_curientloginpass >= 0 && _curientloginpass < Base.InsBase.list.Count)
                    return _curientloginpass;
                else
                {
                    _curientloginpass = -1;
                    return _curientloginpass;
                }
            }
            set
            {
                if (value >= 0 && value < Base.InsBase.list.Count)
                    _curientloginpass = value;
                else _curientloginpass = -1;
            }
        }

        public Int64 StartTime
        {
            get { return _starttime; }
            set { _starttime = value; }
        }

    }
}
