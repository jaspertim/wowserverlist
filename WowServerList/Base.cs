﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace WowServerList
{
    [Serializable]
    public struct loginPass
    {
         [XmlAttribute("login")]
         public string login { get; set; }
         [XmlAttribute("password")]
         public string pass { get; set; }
    }
    [Serializable]
    public class Base
    {
        [XmlIgnore]
        private static Base _base=new Base();
        [XmlElement("Server")]
        public List<string> ServerList = new List<string>();
        [XmlElement("LoginAndPass")]
        public List<loginPass> list=new List<loginPass>();
        [XmlIgnore]
        public static Base InsBase
        {
            get
            {               
                return _base;
            }
            set { _base = value; }

        }
        public void Changelist(int id,loginPass LG)
        {
            list[id] = LG;
        }

        public void ChangeServer(int id, string server)
        {
            ServerList[id] = server;
        }
        public void AddloginPass(string login, string pass)
        {
            list.Add(new loginPass(){login=login, pass=pass});
        }
        public void AddloginPass(loginPass lg)
        {
            list.Add(lg);
        }

        public void DeleteUser(int id)
        {
            list.RemoveAt(id);
        }

        public void DeleteServer(int id)
        {
            ServerList.RemoveAt(id);
        }
        public void AddServer(string server)
        {
        ServerList.Add(server);
        }
    }
}
