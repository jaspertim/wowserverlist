﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WowServerList
{
    /// <summary>
    /// Логика взаимодействия для ListItems.xaml
    /// </summary>
    public partial class ListItems : Window
    {
        private Base B=new Base();
        private SerializeXml Sxml;

        public Base b
        {
            get { return B; }
        }
        public ListItems()
        {
            InitializeComponent();
            this.B = Base.InsBase;
            ListBox_Both();
        }
        /// <summary>
        /// Обвновление списка пользователей
        /// </summary>
        void ListBox_CreateUsers()
        {
            ListBoxUsers.Items.Clear();
            foreach (var LaP in B.list)
            {
                ListBoxItem item=new ListBoxItem()
                {
                    Content=LaP.login,
                    Tag = LaP
                };
                ListBoxUsers.Items.Add(item);
            }
            if (ListBoxUsers.Items.Count > 0) ListBoxUsers.SelectedIndex = 0;
        }
        /// <summary>
        /// /// Обвновление списка серверов
        /// </summary>
        void ListBox_CreateServers()
        {
            ListBoxServers.Items.Clear();
            foreach (var Server in B.ServerList)
            {
                ListBoxItem item = new ListBoxItem()
                {
                    Content = Server
                };
                ListBoxServers.Items.Add(item);
            }
            if (ListBoxServers.Items.Count > 0) ListBoxServers.SelectedIndex = 0;
        }
        /// <summary>
        /// Объединение двух функций ListBox_CreateServers() и ListBox_CreateUsers()
        /// </summary>
        void ListBox_Both()
        {
            ListBox_CreateServers();
            ListBox_CreateUsers();
        }
        /// <summary>
        /// Кнопка добавить элемент
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonAdd_OnClick(object sender, RoutedEventArgs e)
        {
            int index = 0;
            ViewElement view=new ViewElement();
            if (Control.SelectedIndex == 0)
            {
               index = ListBoxUsers.SelectedIndex;
               view=ViewElement.Users;
               ItemWindow iw = new ItemWindow(ItemChoice.Add,  index, view);
               iw.ShowDialog();
               ListBox_Both();
            }

            if (Control.SelectedIndex == 1)
            {
                index = ListBoxServers.SelectedIndex;
                view = ViewElement.Server;
                ItemServer IS = new ItemServer(ItemChoice.Add, index, view);
                IS.ShowDialog();
                ListBox_Both();
            }



        }
        /// <summary>
        /// Кнопка редактировать элемент
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonChange_OnClick(object sender, RoutedEventArgs e)
        {
            int index = 0;
            ViewElement view = new ViewElement();
            if (Control.SelectedIndex == 0)
            {
                if (ListBoxUsers.SelectedIndex != -1)
                {
                    index = ListBoxUsers.SelectedIndex;
                    view = ViewElement.Users;
                    ItemWindow iw = new ItemWindow(ItemChoice.Edit, index, view);
                    iw.ShowDialog(); 
                }

            }

            if (Control.SelectedIndex == 1)
            {
                if (ListBoxServers.SelectedIndex != -1)
                {

                index = ListBoxServers.SelectedIndex;
                view = ViewElement.Server;
                ItemServer IS = new ItemServer(ItemChoice.Edit, index, view);
                IS.ShowDialog();
                ListBox_Both();
                }
            }
                ListBox_Both();
        }
        /// <summary>
        /// Кнопка удаления элемента
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonDelete_OnClick(object sender, RoutedEventArgs e)
        {

            switch (Control.SelectedIndex)
            {
                case 0:
                    if (ListBoxUsers.SelectedIndex != -1)
                    B.DeleteUser(ListBoxUsers.SelectedIndex);
                    break;
                case 1:
                    if (ListBoxServers.SelectedIndex != -1)
                    B.DeleteServer(ListBoxServers.SelectedIndex);
                    break;
            }
        ListBox_Both();
        }
         
        private void ButtonSave_OnClick(object sender, RoutedEventArgs e)
        {
            Sxml = new SerializeXml();
            Sxml.SerializeToXML(B,Sxml.FilenameBase);
        }
    }
}
