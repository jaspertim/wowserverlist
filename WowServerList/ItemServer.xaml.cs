﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WowServerList
{
    /// <summary>
    /// Логика взаимодействия для ItemServer.xaml
    /// </summary>
    public partial class ItemServer : Window
    {
        Base B = new Base();
        private int _id;
        private ViewElement _viewElement;
        private ItemChoice _choice;

        public ItemServer(ItemChoice choice, int idelement, ViewElement view)
        {
            InitializeComponent();
            this.B = Base.InsBase;
            this._id = idelement;
            this._viewElement = view;
            this._choice = choice;

            switch (_choice)
            {
                case ItemChoice.Add:
                    Enter.Content = "Добавить";
                    this.Title = "Добавить элемент";
                    Uri iconUriAdd = new Uri("pack://application:,,,/Icons/gtk-add_5273.ico", UriKind.RelativeOrAbsolute);
                    this.Icon = BitmapFrame.Create(iconUriAdd);
                    break;

                case ItemChoice.Edit:
                    Enter.Content = "Редактировать";
                    this.Title = "Редактировать элемент";
                    TextBoxServer.Text = B.ServerList[_id];
                    Uri iconUriChange = new Uri("pack://application:,,,/Icons/edit_2944.ico", UriKind.RelativeOrAbsolute);
                    this.Icon = BitmapFrame.Create(iconUriChange);
                    break;
            }           
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            switch (_choice)
            {
                case ItemChoice.Add:
                    if (_viewElement == ViewElement.Server)
                    {
                        string server;
                        server = TextBoxServer.Text;
                        B.AddServer(server);
                    }
                    break;
                case ItemChoice.Edit:
                    if (_viewElement == ViewElement.Server)
                    {
                        string server;
                        server = TextBoxServer.Text;
                        B.ChangeServer(_id,server);
                    }
                    break;
            }
            Close();
        }
    }
}
