﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace WowServerList
{
    public class SerializeXml
    {
        public string FilenameBase = @"./Base.xml";
        public string FilenameOption = @"./Option.xml";
        public SerializeXml()
        {
        }
        /// <summary>
        /// Сериализация ХМЛ
        /// </summary>
        public void SerializeToXML(object obj,string Filename)
        {
            XmlSerializer serializer = new XmlSerializer(obj.GetType());
            using (Stream s = File.Create(Filename))
                serializer.Serialize(s, obj);
        }
        /// <summary>
        /// Десериализация ХМЛ
        /// </summary>
        public object DeserializeFromXML(object obj, string Filename)
        {
            XmlSerializer deserializer = new XmlSerializer(obj.GetType());
            if(File.Exists(Filename))
            using (Stream s = File.OpenRead(Filename))
                obj = (object)deserializer.Deserialize(s);
            else
                SerializeToXML(obj,Filename);

            return obj;
        }
    }
}
