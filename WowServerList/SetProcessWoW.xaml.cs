﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using LibProject.WowProcess;

namespace WowServerList
{

    /// <summary>
    /// Логика взаимодействия для SetProcessWoW.xaml
    /// </summary>
    public partial class SetProcessWoW : Window
    {
        private List<ViewModeProcess.ViewModelProc> wowProcesses;

        public Process selectProcess
        {
            get { return ((ViewModeProcess.ViewModelProc)ComboBoxIdProcess.SelectedValue).source; }
        }

        private int selectProcessId
        {
            get { return selectProcess.Id; }
        }

        public SetProcessWoW()
        {
            InitializeComponent();
            wowProcesses = ViewModeProcess.ConvertProcesInViewModel(Proc.GetProcessesByName("WoW"));
            ComboBoxIdProcess.ItemsSource = wowProcesses;

        }

        private void ButtonFocus_OnClick(object sender, RoutedEventArgs e)
        {
            Proc.SetForegroundWindow(selectProcess.MainWindowHandle);
        }

        private void ButtonSelect_OnClick(object sender, RoutedEventArgs e)
        {
            Proc.SetActiveProcess(selectProcess);
            DialogResult = true;
        }
    }
}
